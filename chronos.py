"""
Python job scheduling for humans for MicroPython
Adapted from scheduler for MicroPython

Removed not usable functions

"""
import time


def now():
    return int(time.time_ns() / 1000000) # TODO use monotonic 


class Chronos(object):

    def __init__(self):
        self.jobs = []

    def run_pending(self):
        runnable_jobs = (job for job in self.jobs if job.should_run)
        for job in sorted(runnable_jobs):
            self._run_job(job)

    def run_all(self, delay_seconds=0):
        for job in self.jobs:
            self._run_job(job)
            time.sleep(delay_seconds)

    def clear(self):
        del self.jobs[:]

    def cancel(self, job):
        try:
            self.jobs.remove(job)
        except ValueError:
            pass

    def pause(self, job):
        job.pause()

    def unpause(self, job):
        job.unpause()

    def every(self, interval=1):
        job = Job(interval)
        self.jobs.append(job)
        return job

    def _run_job(self, job):
        ret = job.run()


class Job(object):
    
    def __init__(self, interval):
        self.interval = interval  # pause interval * unit between runs
        self.job_func = None  # the job job_func to run
        self.unit = None  # time units, e.g. 'minutes', 'hours', ...
        self.last_run = None  # time of the last run
        self.next_run = None  # time of the next run
        self.period = None  # timedelta between runs, only valid for
        
    def __lt__(self, other):
        """PeriodicJobs are sortable based on the scheduled time
        they run next."""
        return self.next_run < other.next_run

    @property
    def miliseconds(self):
        self.unit = 'miliseconds'
        return self

    @property
    def second(self):
        assert self.interval == 1
        return self.seconds

    @property
    def seconds(self):
        self.unit = 'seconds'
        return self

    @property
    def minute(self):
        assert self.interval == 1
        return self.minutes

    @property
    def minutes(self):
        self.unit = 'minutes'
        return self

    @property
    def hour(self):
        assert self.interval == 1
        return self.hours

    @property
    def hours(self):
        self.unit = 'hours'
        return self

    def pause(self):
        self.next_run = now() * 2

    def unpause(self):
        self._schedule_next_run()

    def do(self, job_func):
        self.job_func = job_func
        self._schedule_next_run()
        return self

    @property
    def should_run(self):
        return now() >= self.next_run

    def run(self):
        ret = None
        self.job_func()
        self.last_run = now()
        self._schedule_next_run()
        return ret

    def _schedule_next_run(self):
        assert self.unit in ('miliseconds','seconds', 'minutes', 'hours')
        f = 1
        if self.unit == 'seconds':
            f = 1000
        elif self.unit == 'minutes':
            f = 60000
        elif self.unit == 'hours':
            f = 60 * 60000
        self.period = f * self.interval
        self.next_run = now() + self.period

        

_chronos = Chronos()


def every(interval=1):
    return _chronos.every(interval)


def govern():
    _chronos.run_pending()


def run_all(delay_seconds=0):
    _chronos.run_all(delay_seconds=delay_seconds)


def clear():
    _chronos.clear()


def cancel(job):
    _chronos.cancel(job)

def pause(job):
    _chrons.pause(job)

def unpause(job):
    _chrons.unpause(job)

